"""Base module for shared across all templates."""
import os

from jinja2 import Environment, FileSystemLoader
from troposphere import Base64, Join, Tags, Template


class SceptreTemplate:
    """Parent class for all Sceptre templates."""

    def __init__(self):  # noqa: D102
        self.template = Template()

    @staticmethod
    def tag_resource(tag_dict):
        """Create tags dict."""
        if tag_dict:
            return {"Tags": Tags(**tag_dict)}
        return {"Tags": Tags()}

    @staticmethod
    def read_from_file(file_path, variables):
        """Read file and return base64 representation."""
        path, filename = os.path.split(file_path)
        render_result = (
            Environment(
                loader=FileSystemLoader(path), trim_blocks=True, lstrip_blocks=True
            )
            .get_template(filename)
            .render(variables)
        )

        rendered_file = os.path.join(path, ".rendered_" + filename)
        with open(rendered_file, "w") as outf:
            outf.write(render_result)

        with open(rendered_file, "r") as inf:
            data = inf.readlines()
        os.remove(rendered_file)

        return Base64(Join("", data))


def parse_resource_tags(tags: dict = None):
    """Convert tags dict into Tags objects."""
    if tags:
        return Tags(**tags)
    return Tags()