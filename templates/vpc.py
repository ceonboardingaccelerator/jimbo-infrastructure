"""Create vpc and optionally internet gateway + vpn gateway."""
from troposphere import Parameter, Ref, Output, GetAtt
from troposphere.ec2 import VPC, InternetGateway, VPNGateway, VPCGatewayAttachment

from sceptre_template import SceptreTemplate


class SceptreResource(SceptreTemplate):
    """Sceptre resource template."""

    def __init__(self, sceptre_user_data):  # noqa: D102
        super(SceptreResource, self).__init__()
        self.sceptre_user_data = sceptre_user_data
        self.vpc_cidr = self.template.add_parameter(
            Parameter("VpcCidr", Description="VPC CIDR allocation", Type="String")
        )
        self.vpc = None
        self.add_vpc()
        if self.sceptre_user_data.get("AttachIgw"):
            self.add_igw()
        if self.sceptre_user_data.get("AttachVpg"):
            self.add_vpg()

    def add_vpc(self):
        """Add vpc to template."""
        vpc_kwargs = {"CidrBlock": Ref(self.vpc_cidr)}
        vpc_kwargs.update(self.sceptre_user_data["VpcProperties"])
        vpc_kwargs.update(
            self.tag_resource(self.sceptre_user_data.get("Tags").get("Vpc"))
        )
        self.vpc = self.template.add_resource(VPC("VPC", **vpc_kwargs))
        self.template.add_output(
            Output("VpcId", Value=Ref(self.vpc), Description="Vpc Id")
        )
        self.template.add_output(
            Output(
                "VpcCidr", Value=GetAtt(self.vpc, "CidrBlock"), Description="Vpc Cidr"
            )
        )
        self.template.add_output(
            Output(
                "VpcRegion",
                Value=Ref("AWS::Region"),
                Description="Region in which the VPC was launched",
            )
        )

    def add_igw(self):
        """Add internet gateway to template."""
        igw_kwargs = {}
        igw_kwargs.update(
            self.tag_resource(self.sceptre_user_data.get("Tags").get("Igw"))
        )
        igw = self.template.add_resource(InternetGateway("IGW", **igw_kwargs))
        attachment_kwargs = {"InternetGatewayId": Ref(igw), "VpcId": Ref(self.vpc)}
        self.template.add_resource(
            VPCGatewayAttachment("IgwAttachment", **attachment_kwargs)
        )
        self.template.add_output(
            Output("IgwId", Value=Ref(igw), Description="Internet Gateway Id")
        )

    def add_vpg(self):
        """Add virtual private gateway to template."""
        vpg_kwargs = {"Type": "ipsec.1"}
        vpg_kwargs.update(
            self.tag_resource(self.sceptre_user_data.get("Tags").get("Vpg"))
        )
        vpg = self.template.add_resource(VPNGateway("VPG", **vpg_kwargs))
        attachment_kwargs = {"VpnGatewayId": Ref(vpg), "VpcId": Ref(self.vpc)}
        self.template.add_resource(
            VPCGatewayAttachment("VpgAttachment", **attachment_kwargs)
        )
        self.template.add_output(
            Output("VpgId", Value=Ref(vpg), Description="Virtual Private Gateway Id")
        )


def sceptre_handler(sceptre_user_data):
    """Render Cloudformation template with defined resources."""
    return SceptreResource(sceptre_user_data).template.to_yaml()
