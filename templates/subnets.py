# pylint: disable=fixme
"""Template to handle basic AWS account subnet networking."""
from troposphere import Output, Parameter, Ref, GetAZs, Select, GetAtt
from troposphere.ec2 import (
    Subnet,
    RouteTable,
    Route,
    EIP,
    SubnetRouteTableAssociation,
    NatGateway,
)

from sceptre_template import SceptreTemplate, parse_resource_tags


class SceptreResource(SceptreTemplate):
    """Sceptre resource template."""

    def __init__(self, sceptre_user_data):  # noqa: D102
        super(SceptreResource, self).__init__()
        self.sceptre_user_data = sceptre_user_data
        self.nat_gateway_list = []
        self.resources = []
        self.param_vpc_id = self.template.add_parameter(
            Parameter("VpcId", Description="VPC ID", Type="String")
        )
        self.param_igw_id = self.template.add_parameter(
            Parameter("IgwId", Description="IGW ID", Type="String")
        )

        self.create_resources()
        self.update_template()

    def create_resources(self):
        """Generate resources and outputs."""
        nat_gateways = {}
        for tier_name, tier_properties in self.sceptre_user_data["Tiers"].items():
            for idx, cird_block in enumerate(tier_properties.get("CidrBlocks")):

                subnet = create_subnet(
                    name_resource(tier_name, idx, "Subnet"),
                    cird_block,
                    availability_zone=Select(idx, GetAZs()),
                    tags=tier_properties.get("Tags"),
                    vpc_id=Ref(self.param_vpc_id),
                )
                route_table = create_route_table(
                    name_resource(tier_name, idx, "RouteTable"),
                    vpc_id=Ref(self.param_vpc_id),
                    tags=tier_properties.get("Tags"),
                )
                route_table_association = create_route_table_association(
                    name_resource(tier_name, idx, "Association"), subnet, route_table
                )

                self.resources.extend((subnet, route_table, route_table_association))
                # add nat gw if this tiers should has it
                if tier_properties.get("NatGateway"):
                    nat_gw_name = name_resource(tier_name, idx, "NatGateway")
                    eip = create_elastic_ip("{}EIP".format(nat_gw_name))
                    nat_gateway = create_nat_gateway(nat_gw_name, eip, subnet)
                    self.resources.append(eip)
                    self.resources.append(nat_gateway)
                    nat_gateways[idx] = nat_gateway

                internet_access = tier_properties.get("InternetAccess")
                if internet_access == "IGW":
                    internet_route = create_route(
                        name_resource(tier_name, idx, "IgwRoute"),
                        "GatewayId",
                        Ref(self.param_igw_id),
                        route_table,
                    )
                    self.resources.append(internet_route)

                elif internet_access == "NAT":
                    # route to nat gateway in NatGW tier by idx
                    internet_route = create_route(
                        name_resource(tier_name, idx, "NatRoute"),
                        "NatGatewayId",
                        Ref(nat_gateways[idx]),
                        route_table,
                    )
                    self.resources.append(internet_route)

    def update_template(self):
        """Add resources and outputs to template."""
        for res in self.resources:
            self.template.add_resource(res)
            self.template.add_output(Output(res.title, Value=Ref(res)))


def name_resource(base: str, idx: int, resource_name: str) -> str:
    """Name resources with given scheme."""
    return "{}{}{}".format(base, resource_name, idx)


def create_subnet(
    resource_name: str, cidr_block: str, availability_zone, vpc_id, tags: dict = None
) -> Subnet:
    """Create troposphere resource subnet."""
    subnet_properties = {
        # "AssignIpv6AddressOnCreation": Boolean,  # FIXME missing property
        "AvailabilityZone": availability_zone,
        "CidrBlock": cidr_block,
        # "Ipv6CidrBlock": String,  # FIXME missing property
        # "MapPublicIpOnLaunch": Boolean,  # FIXME missing property
        "Tags": parse_resource_tags(tags),
        "VpcId": vpc_id,
    }

    return Subnet(resource_name, **subnet_properties)


def create_route_table(resource_name: str, vpc_id, tags: dict = None) -> RouteTable:
    """Create troposphere routing table."""
    route_table_properties = {"VpcId": vpc_id, "Tags": parse_resource_tags(tags)}
    return RouteTable(resource_name, **route_table_properties)


def create_route(
    resource_name: str, gwid_type: str, gw_id, route_table: RouteTable
) -> Route:
    """Create troposphere route."""
    # possible enhancement to check IPv type(4,6)
    gateways = (
        "EgressOnlyInternetGatewayId",  # String
        "GatewayId",  # String
        "InstanceId",  # String,
        "NatGatewayId",  # String,
        "NetworkInterfaceId",  # String,
        "VpcPeeringConnectionId",  # String
    )
    if gwid_type not in gateways:
        raise TypeError("Gateway type not in: {}".format(gateways))

    route_properties = {
        "DestinationCidrBlock": "0.0.0.0/0",  # String
        # TODO only IPv4 or 6 can be specified, Troposphere exception
        # "DestinationIpv6CidrBlock": "",  # String
        "RouteTableId": Ref(route_table),  # String
        gwid_type: gw_id,
    }

    return Route(resource_name, **route_properties)


def create_route_table_association(
    resource_name: str, subnet: Subnet, route_table: RouteTable
) -> SubnetRouteTableAssociation:
    """Create route table subnet association troposphere resource."""
    association_properties = {"RouteTableId": Ref(route_table), "SubnetId": Ref(subnet)}
    return SubnetRouteTableAssociation(resource_name, **association_properties)


def create_elastic_ip(resource_name: str) -> EIP:
    """Create elastic IP troposphere resource."""
    return EIP(resource_name, Domain="vpc")


def create_nat_gateway(
    resource_name: str, elastic_ip: EIP, subnet: Subnet
) -> NatGateway:
    """Create nat gateway troposphere resource."""
    nat_gateway_properties = {
        "AllocationId": GetAtt(elastic_ip, "AllocationId"),
        "SubnetId": Ref(subnet),
    }
    return NatGateway(resource_name, **nat_gateway_properties)


def sceptre_handler(sceptre_user_data):
    """Render Cloudformation template with defined resources."""
    return SceptreResource(sceptre_user_data).template.to_yaml()