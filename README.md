**Jimbo Project**

Jimbo Project aims to deliver out of the box, secure and ready to use Jenkins ecosystem, so that consumer can rapidly start implementing business logic and process without need to spend time on infrastructure setup.

*Full documentation can be found [here](https://cloudreach.jira.com/wiki/spaces/CE/pages/1732477926/Jimbo+design+document).*

---

## Jimbo Project infrastrcture overview

Jimbo Project consists of:

1. Network layer full setup
2. Compute resources provision
3. Jenkins service automated configuration  


---

## Deployment guide

In order to start development of new feature follow below steps:

- Create PR url - https://bitbucket.org/ceonboardingaccelerator/jimbo-infrastructure/pull-requests/new

- Verify deployment url - https://bitbucket.org/ceonboardingaccelerator/jimbo-infrastructure/addon/pipelines/home#!/

0. Cut off new feature branch with master as base
```
# git checkout master # make sure you're at master branch
# git checkout -b feature/adding-new-ec2-instance-for-jenkins # deployment workflow mandates the following naming convention 'feature/<your-change-short-summary>'
```
2. Start development
3. In order to verify if new change can be deployed to development - create a PR from your feature branch to develop branch (this will trigger automated stage run - deployment dry-run on development)
4. Upon successful run of previous step **3.** - merge PR (any commits to develop branch trigger actual deployment to development environment)
5. Verify step **4.** stage outputs and check if the desired changes have been made on target environment
6. Create a new PR from develop to master (this will trigger automated stage run - deployment dry-run on production)
7. Merge PR from step **6.** (any commits to master branch trigger actual deployment to production environment)
8. Verify production deployment status

---

**Local project setup**

Before start - install required python packages
```
pip install -r requirements.txt
```

Deploy multi stack infrastructure using Sceptre 
```
sceptre --var-file=variables.yaml create development/network
```

Deploy infrastructure by single stack using Sceptre 
```
sceptre --var-file=variables.yaml create development/network/vpc.yaml
```

In order to deploy infrastrucure changes do the following
```
sceptre --var-file=variables.yaml create development/network/vpc.yaml <name-of-your-change-set>
# view change set in CF UI
# deploy changes using the execute like below
sceptre --var-file=variables.yaml execute development/network/vpc.yaml <name-of-your-change-set>
```

Verify changes made
---

## To Do

This project is **under development** phase and below tasks are still pending completion

1. Enable [pre-commit hooks](https://github.com/pre-commit/pre-commit-hooks)
2. Build production VPC
3. Build Jenkins master on EC2
4. Develop custom scripting so that pipelines can deploy (create) non existing stack - as for now it only support deploying change sets
5. Update VPCs naming convention from "Jimbo" to "development-vpc" and "production-vpc"
